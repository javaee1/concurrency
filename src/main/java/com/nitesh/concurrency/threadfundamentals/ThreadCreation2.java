package com.nitesh.concurrency.threadfundamentals;

public class ThreadCreation2 {
	
	public static void main(String[] args) {
		
		var thread = new NewThread();
		thread.setName("workerThread");
		thread.start();
		
	}

	private static class NewThread extends Thread{
		
		@Override
		public void run() {
			System.out.println("Hello From Thread: "+ this.getName());
		}
		
	}
}
