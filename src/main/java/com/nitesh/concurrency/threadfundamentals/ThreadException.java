package com.nitesh.concurrency.threadfundamentals;

public class ThreadException {
	
	public static void main(String[] args) {
		
		Thread thread  =  new Thread(new Runnable() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				throw new RuntimeException("Intentional Error is thrown");
				
			}
		});
		
		thread.setName("WorkerThread");
		thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
			@Override
			public void uncaughtException(Thread arg0, Throwable arg1) {
				System.out.println("A critical exception is occured: " + arg0.getName()
				+ " and the message is " + arg1.getMessage());
			}
		});
		
		System.out.println("Main Thread Runnig before worker Thread.");
		thread.start();
		System.out.println("Main Thread Runnig after worker Thread.");
	}

}
