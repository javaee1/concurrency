package com.nitesh.concurrency.threadfundamentals;

/*
 * ps -aux | grep MultiThreadSpan
 * ps -o nlwp 6466
 * cat /proc/6466/status | grep Threads
 */

import java.util.ArrayList;
import java.util.List;

public class MultiThreadSpan {
	
	public static void main(String[] args) {
		
		List<Thread> threads = new ArrayList<>();
		for(int i=0;i<4;i++) {
			threads.add(new MessagePrinterThread("Nitesh "+ i));
		}
		
		for(Thread thread : threads) {
			thread.start();
		}
	}
	
	private static class MessagePrinterThread extends Thread{
		
		private String message;
		
		public MessagePrinterThread(String message) {
			this.message = message;
		}
		
		@Override
		public void run() {
			while(true) {
				System.out.println("Message: " + message);
			}
		}
	}

}
