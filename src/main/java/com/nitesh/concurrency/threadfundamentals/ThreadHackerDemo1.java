package com.nitesh.concurrency.threadfundamentals;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class ThreadHackerDemo1 {

	public static final int MAX_PASSWORD = 9999;

	public static void main(String[] args) {
		
		Random random = new Random();
		Vault vault = new Vault(random.nextInt(MAX_PASSWORD));
		
		List<Thread> threadList = new ArrayList<>();
		threadList.add(new AscendingHackerThread(vault));
		threadList.add(new DecendingHackerThread(vault));
		threadList.add(new PoliceThread());
		
		threadList.forEach(arg->arg.start());

	}

	public static class Vault {
		private int password;

		public Vault(int pass) {
			this.password = pass;
		}

		public boolean isCorrectPassword(int guess) {
			try {
				Thread.sleep(5);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return this.password == guess;
		}
	}

	public static abstract class HackerThread extends Thread {

		protected Vault vault;

		public HackerThread(Vault vault) {
			this.vault = vault;
			this.setName(this.getClass().getSimpleName());
			this.setPriority(Thread.MAX_PRIORITY);
		}

		@Override
		public void start() {
			System.out.println("Starting thread " + this.getName());
			super.start();
		}

	}

	public static class AscendingHackerThread extends HackerThread {

		public AscendingHackerThread(Vault vault) {
			super(vault);
		}

		@Override
		public void run() {
			for (int guess = 0; guess < MAX_PASSWORD; guess++) {
				if (vault.isCorrectPassword(guess)) {
					System.out.println(this.getName() + " guessed the password " + guess);
					System.exit(0);
				}
			}
		}

	}

	public static class DecendingHackerThread extends HackerThread {

		public DecendingHackerThread(Vault vault) {
			super(vault);
		}

		@Override
		public void run() {
			for (int guess = MAX_PASSWORD; guess >= 0; guess--) {
				if (vault.isCorrectPassword(guess)) {
					System.out.println(this.getName() + " guessed the password " + guess);
					System.exit(0);
				}
			}

		}
	}
	
	public static class PoliceThread extends Thread {
		
		@Override
		public void start() {
			System.out.println("Police thread started");
			super.start();
		}
		
		@Override
		public void run() {
			for(int i=10;i>0;i--) {
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				System.out.println(i);
			}
			
			System.out.println("Game over for you Hackers");
			System.exit(0);
		}
	}
}



