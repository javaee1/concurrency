package com.nitesh.concurrency.threadfundamentals;

public class ThreadCreation1 {
	
	public static void main(String[] args) {
		
		Thread thread = new Thread(new Runnable() {
			
			@Override
			public void run() {
				System.out.println("The Current Thread is Running is :" + Thread.currentThread().getName());
				System.out.println("The Priority of Current Thread is: " + Thread.currentThread().getPriority());
			}
		});
		
		thread.setName("New Worker Thread");
		thread.setPriority(Thread.MAX_PRIORITY);
		
		System.out.println("Before executing Worker Thread thread name: " + Thread.currentThread().getName());
		thread.start();
		System.out.println("After executing  Worker Thread thread name: " + Thread.currentThread().getName());
	}
	
}
