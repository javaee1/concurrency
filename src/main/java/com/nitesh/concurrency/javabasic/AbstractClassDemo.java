package com.nitesh.concurrency.javabasic;

abstract class Programming {
	
	protected String language;
	public static final String author= "Nitesh Nandan";
	
	public static void notes() {
		System.out.println("Keep Calm and Code");
	}
	
	public Programming(String lang) {
		this.language = lang;
	}
	
	public abstract void learn();
}

class PythonProgramming extends Programming{

	public PythonProgramming(String lang) {
		super(lang);
	}

	@Override
	public void learn() {
		System.out.println("I am learning " + this.language);		
		System.out.println(Programming.author);
		Programming.notes();
	}
	
	
	
}

public class AbstractClassDemo {
	
	public static void main(String[] args) {
		
		Programming programming = new Programming("java") {
			@Override
			public void learn() {
				System.out.println("I am learning " + this.language);
				
			}
		};
		
		programming.learn();
		
		var pyprogram = new PythonProgramming("Python");
		pyprogram.learn();
		
		System.out.println(Programming.author);
		Programming.notes();
	}

}
