 package com.nitesh.concurrency.threadinterrupt;

import java.io.IOException;

public class ThreadInterruptionDemo2 {
	
	public static void main(String[] args) {
		
		Thread th = new Thread(new DemonThread());
//		th.setDaemon(true);
		th.start();
		
	}
	
	private static class DemonThread implements Runnable{

		@Override
		public void run() {
			while(true) {
				System.out.println("Dameon Thread is Runnig");
				try {
					char input = (char)System.in.read();
					if(input=='q') {
						System.out.println("Exiting the thread Q is pressed.");
						return;
					}
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
}
