 package com.nitesh.concurrency.threadinterrupt;

public class ThreadInterruptDemo1 {
	
	public static void main(String[] args) {
		
		Thread thread =  new Thread(new BlockingThread());
		thread.start();
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		thread.interrupt();
	}
	
	
	private static class BlockingThread implements Runnable{

		@Override
		public void run() {
			
			while(true) {
				// Thread.interrupted return boolean value, if thread is interrupted or not
				// if this line is not added then no way to interrupt the thread.
				// A thread can only interrupted only if a function throws interrupted exception
				// or we explicitly handle the interruption.
				if(Thread.currentThread().isInterrupted()) {
					System.out.println("Existing The thread");
					return;
				}
				System.out.println("echo ");
			}
			
		}
	}

}
