package com.nitesh.concurrency.resourcesharing;

public class ResourceSharingChallengeDemo {
	
	
	public static void main(String[] args) throws InterruptedException {
		
		SharedResource sharedResource = new SharedResource();
		Thread incThread = new IncrementThread(sharedResource, 10000);
		Thread decThread = new DecrementThread(sharedResource, 10000);
		
		incThread.start();
		decThread.start();
		
		incThread.join();
		decThread.join();
		
		System.out.println("Value is: " + sharedResource.getValue());
 	}
	
	public static class SharedResource {
		private int value = 0;
		
		public void increment() {
			this.value++;
		}
		
		public void decrement() {
			this.value--;
		}
		
		public int getValue() {
			return this.value;
		}
	}
	
	
	public static class IncrementThread extends Thread {
		
		private SharedResource sharedResource;
		private int value;
		
		public IncrementThread(SharedResource sharedResource, int value) {
			this.sharedResource = sharedResource;
			this.value = value;
		}
		
		@Override
		public void run() {
			for(int i=0;i<value;i++) {
				try {
					Thread.sleep(10);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				sharedResource.increment();
			}
		}
	}
	
public static class DecrementThread extends Thread {
		
		private SharedResource sharedResource;
		private int value;
		
		public DecrementThread(SharedResource sharedResource, int value) {
			this.sharedResource = sharedResource;
			this.value = value;
		}
		
		@Override
		public void run() {
			for(int i=0;i<value;i++) {
				try {
					Thread.sleep(10);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				sharedResource.decrement();
			}
		}
	}
}
