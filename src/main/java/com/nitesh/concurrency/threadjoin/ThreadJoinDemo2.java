package com.nitesh.concurrency.threadjoin;

import java.util.ArrayList;
import java.util.List;

public class ThreadJoinDemo2 {
	
	public static void main(String[] args) {
		
		List<Thread> threads = new ArrayList<>();
		
		for(int i=0;i<10;i++) {
			threads.add(new Thread(new WorkerThread("Thread No: "+i)));
		}
		
		for(int i=0;i<10;i++) {
			threads.get(i).start();
		}
		
		long startTime = System.currentTimeMillis();
		for(Thread thread : threads) {
			try {
				thread.join();
//				thread.join(20);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		long endTime = System.currentTimeMillis();
		
		System.out.println("Time to taken to finishup the work is " + ((endTime-startTime)));
		
		System.out.println("Finished");
		
	}
	
	private static class WorkerThread implements Runnable {
		
		private String message;
		
		public  WorkerThread(String message) {
			this.message = message;
		}

		@Override
		public void run() {
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			
			System.out.println("Message From Woker Thread " + this.message);
		}
	}

}
