# **Concurrency**

## **Fundamentals**


![Ways to Create Thread](.doc/resource/img/img1_2.png)

![Ways to Create Thread](.doc/resource/img/img1_3.png)

![Ways to Create Thread](.doc/resource/img/img1_4.png)

![Ways to Create Thread](.doc/resource/img/img1_5.png)

![Ways to Create Thread](.doc/resource/img/img1_6.png)

---

## **Thread Creation**


![Ways to Create Thread](.doc/resource/img/twoWaysToCreateThread.png)

![Summary](.doc/resource/img/img2_1.png)

---

## **Thread Interrupt**


![Thread Termination Why and When?](.doc/resource/img/img2_2.png)

![Thread Interrupt](.doc/resource/img/img2_5.png)

![Daemon Thread](.doc/resource/img/img2_3.png)

![Ways to Create Thread](.doc/resource/img/img2_4.png)

---

## **Thread Coordination**

![Thread Coordination- why do we need it?](.doc/resource/img/img3_1.png)

![Thread Dependency](.doc/resource/img/img3_2.png)

![Thread Coordination Naive solution](.doc/resource/img/img3_3.png)

![Thread coordination naive solution 1](.doc/resource/img/img3_6.png)

![Thread Coordination Desired Solution](.doc/resource/img/img3_4.png)



![hread coordination Summary](.doc/resource/img/img3_5.png)

![Thread join](.doc/resource/img/img3_7.png)

---

## **Performance In Multithreading**


![Performance in Multithreading](.doc/resource/img/img4_2.png)

![Latency](.doc/resource/img/img4_3.png)


![Latency 2](.doc/resource/img/img4_5.png)


![No of thread](.doc/resource/img/img4_7.png)

![Inherent Cost of parallelization and aggregation (latency)](.doc/resource/img/img4_8.png)

![Tradeoff graph](.doc/resource/img/img4_9.png)

![Throughput Defination](.doc/resource/img/img4_10.png)

![Approach 1](.doc/resource/img/img4_11.png)

![Approach 2](.doc/resource/img/img4_12.png)

![Approach 2 cost cutting](.doc/resource/img/img4_13.png)

---

## **Data Sharing between Threads**

![Allocation of Data](.doc/resource/img/img5_1.png)

![Question 1](.doc/resource/img/img5_2.png)

![Question 2](.doc/resource/img/img5_3.png)

![Question 3](.doc/resource/img/img5_5.png)

![Question 4](.doc/resource/img/img5_6.png)

![What is Resource](.doc/resource/img/img5_7.png)

---

## **Synchronization and Critical Section**


![Atomic Operation](.doc/resource/img/img6_1.png)

![Race conditon Intro](.doc/resource/img/img6_2.png)

![Critical Section](.doc/resource/img/img6_3.png)

![Critical Section 2](.doc/resource/img/img6_4.png)


![Synchronized method](.doc/resource/img/img6_6.png)

![synchronized lock](.doc/resource/img/img6_7.png)

![Advantage of Synchronized Lock](.doc/resource/img/img6_9.png)